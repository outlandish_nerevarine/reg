#!/usr/bin/env python3
# encoding: utf-8
import json
from time import sleep

import npyscreen
import requests
import re
import datetime

crow = """
                    .-`    `o+::                                                
              `.+so+oyhs+::shhdh+:. `o-`..`                                     
          `/syyyyyhhhhhhhhhddmmdhhhoodhyhdh+                                    
         .sdhyhhhhhhhyhhhhdmddhhhhhhhhhhhhhh/`                                  
        `+hhhddy++dmdhhhhhmmhyhhddhyssyhdhhdds.                                 
        -hhso:` `smdyhhhhdmddmmddysyyyssddyyhs-                                 
        :s.      /dm-:hhhdmmmdddsydhssssydhhso:                                 
                  -d/:hddhhhdhysyyhysssshNh-                                    
                  `sh:ydsssssssssyhsssyhdd+                .-.`  -oy.           
                 -yhhhsssssssssyhyssydddhho..`````````````+hhhh+:shyh+:-        
                 .sdysssssssyhhyssydddhdhhy+o++syyoo++o+///++-/:/ohhhhhhs       
                  +yssssssyhhssyhddhhhhhdhhhhhyssososoo+:/:-/ooo+++shdhdyoo+-   
                 .sssssshhyssyhddhhhhyyyo+//:/+oyyhhhhddy+oyo+o+:-.:sddhyyyhh/  
                 .sssshhssyys+::s/////+ossyhdddhhhhhhhyhddhdds:/syhhhho-`  ``   
    :++++-` `-/- `+sshyso+:`.+shhhhhhhhhhyhhhhyyyhhhhhhhhddhhdmdhhhhdh:         
   .osyhyhs/yhhhy/`:/`       `:dhhhyhdhhdhhyhhhhhhhhhhhhhdddhhddyyhhhhh-        
      `:yddhyhhhhds/`        -yhhhhhhhhddmmdhhhdddhhhhyhddhshhhdhhhhhhhh`       
        +hyyhhhhhhhdo.`     `ohhhhhhhhhhyhhhhdddddhdddhhmdy./ddhhhhhhhhh:       
        -yhhhhyhhddhhh+` `-:sdhhhhhhdh//yhhhhhyhhhhhhhhdhho` /hhdhhhhhhd.       
         `:hhhhyyyyhhd+:syhhyhhdhyhhdh-  -ohhhhhhhhhhhhyhh-   ` :shdddd/        
           `oddhhyhddhyhhhhhhhhhhhyhds.    .hhhhhhhhhhhyh+        `--.`         
             `+hdhdhhhyhhhhhhhhhhhhhh:     -dhhhhhhhhhyho`                      
               `:oyhdhhhhhhhhhyhhdhhy.    -hhhhhhhhhhyho                        
                   `-/shhhhhhhhyhdmh:   .ohhyhhhhhhhhh+`                        
                       `oddhhhddhhd/ `:sdhyhhhhhhhhhs.                          
                         -s//osys/yyohddhyyhyoydhhyd-                           
                              `.oyhhhdhyhhho``sdhhyd`                           
                             -ydhhhddhyhhh+` :hdhhyd-                           
                            :hhhhdmdhyhhho` `sdhhhyh+                           
                           /hhhhdddhyhhyy.  +dhhhhhds                           
                         `+ddddhddhhyhyd-  `ddhhdddh/                           
                         `//:---hmdhhhh/    sddhy+-.                            
                               -hyyho`      :hyso                               
                              -yyyho        .yhys`                              
                             .yysso`        .yysy.                              
                            `shyso.         `ohyh/                              
                            /hsss-           /yys+`                             
                        `` -yyss:           `/yyss-       `:s/:+.               
                      .yhsosyyy:        `..:/syysso- ``.-:+ssysss+:`            
                      `syhyyyys-      `:ydyyyyyssssssooyyyssssssssyy.           
                       `/hyyysss:`   `odyssyyssssssyyssshysssssssssys.          
                       .dyyyysssss+:.-+/ddhyyoosyyhdddhhyysssyyyyssh.           
                       omsysssssssssydo```                   `...``.            
                       sNyyyssssssssydy`                                        
                      :ddhyyyyssssoosy/                                         
                     `:-o:. -:.-:.        
"""

"""
* CONFIG *
"""

# prints the FTM crow and has prompts to continue
PRINT_WELCOME_SCREEN = False

# Don't let the user proceed until all input appears valid
VALIDATION_ENABLED = True
EMAIL_REGEX = re.compile("^[^@]+@[^@]+\.[^@]+$")

# on-site registration URLs
REG_URL = "http://dawningbrooke.net/apis/registration/onsite"
CART_URL = "http://dawningbrooke.net/apis/registration/cart/add"
CHECKOUT_URL = "http://dawningbrooke.net/apis/registration/cart/checkout"
REG_LEVELS_URL = "http://dawningbrooke.net/apis/registration/pricelevels"

# if the levels can't be grabbed for some reason, it'll fall back to using this cached version
REG_LEVELS_CACHED = [{"description": "After pre-reg", "base_price": "50.00", "options": [{"name": "Con Book", "required": False, "list": [], "value": "0.00", "active": True, "type": "bool", "id": 2}], "name": "Attendee", "id": 26}, {"description": "All Sponsors receive the 2018 Collectable pin.\r\n<p>&nbsp;</p>\r\n<p>Select your additional options below:</p>", "base_price": "90.00", "options": [{"name": "T-shirt", "required": True, "list": [{"name": "Small", "id": 1}, {"name": "Medium", "id": 2}, {"name": "Large", "id": 3}, {"name": "Extra Large", "id": 4}, {"name": "2x", "id": 5}, {"name": "3x", "id": 6}], "value": "0.00", "active": True, "type": "ShirtSizes", "id": 1}, {"name": "Con Book", "required": False, "list": [], "value": "0.00", "active": True, "type": "bool", "id": 2}], "name": "Sponsor", "id": 4}]

REQ_HEADERS = {'User-Agent': 'FTM 2018 On-site Registration Script V1 - Foxipso'}

"""
* END CONFIG *
"""

class RegistrationForm(npyscreen.ActionForm):
    def on_ok(self):
        if npyscreen.notify_yes_no("Are you ready to submit your information?", title="Use tab and arrow keys to select"):
            pass
        else:
            self.editing = True

def validPhone(phone):
    #Phone regex ended up being too variable
    if strOutside(phone, 1, 20):
        return False

    return True

def validEmail(email):
    if strOutside(email, 1, 50) or not EMAIL_REGEX.match(email):
        return False

    return True

def strOutside(input, min, max):
    """
    Is len(input) within min, max, inclusive?
    """
    if len(input) >= min and len(input) <= max:
        return False

    return True

class TestApp(npyscreen.NPSApp):
    session = None
    levels = {}
    editing = False

    def getLevels(self):
        r = requests.get(REG_LEVELS_URL, headers=REQ_HEADERS)
        return r.json()

    def isValid(self):
        """
        Checks to see if the information in the first form is valid.
        If it's not valid, it displays a dialog indicating the errors

        :return: True if valid, False otherwise
        """
        validation_string = ""

        if strOutside(self.first.value, 1, 50) or strOutside(self.last.value, 1, 50):
            validation_string += "Name is invalid. "
        if not validEmail(self.email.value):
            validation_string += "Email is invalid. "
        if not validPhone(self.phone.value):
            validation_string += "Phone is invalid. "
        if strOutside(self.add1.value, 1, 100) or strOutside(self.add2.value, 0, 100):
            validation_string += "Address 1 or 2 is invalid. "
        if strOutside(self.city.value, 1, 50):
            validation_string += "City is invalid. "
        if strOutside(self.state.value, 1, 50):
            validation_string += "State is invalid. "
        # International ZIP codes get weird. Best we can do
        if strOutside(self.zip.value, 1, 30):
            validation_string += "ZIP is invalid. "
        if strOutside(self.country.value, 1, 100):
            validation_string += "Country is invalid. "
        try:
            if self.dob.value is None or self.dob.value > datetime.datetime.now().date():
                validation_string += "DOB is invalid. "
        except:
            validation_string += "DOB is invalid. "

        if validation_string == "":
            return True

        npyscreen.notify_confirm(validation_string, title="Use tab and arrow keys")
        return False

    def isAdditionalValid(self):
        validation_string = ""

        if strOutside(self.badge.value, 1, 100):
            validation_string += "Your badge name is too empty or too long. "

        if validation_string == "":
            return True

        npyscreen.notify_confirm(validation_string, title="Use tab and arrow keys")
        return False


    def main(self):
        try:
            self.session = requests.Session()
            self.session.get(REG_URL, headers=REQ_HEADERS)
        except Exception as e:
            npyscreen.notify_confirm("An exception occurred while trying to get the CSRF cookie! Proceeding is likely a bad idea. Please report the following error: " + str(e), title="Use tab and arrow keys")

        try:
            self.levels = self.getLevels()
        except Exception as e:
            npyscreen.notify_confirm("An exception occurred while trying to get the subscription levels! Defaulted to cached version. " + str(e), title="Use tab and arrow keys")
            self.levels = REG_LEVELS_CACHED

        form  = RegistrationForm(name = "Use tab, arrow keys, and spacebar")

        OPT_OUT_CON_INFO    = "Con-related information"
        OPT_OUT_SURVEY      = "Post-con survey"

        try:
            form.add(npyscreen.FixedText, value="Full (Legal) Name", rely=2, relx=4)
            self.first   = form.add(npyscreen.TitleText, name="First:", rely=3, relx=6, width=30, begin_entry_at=9)
            self.last    = form.add(npyscreen.TitleText, name="Last:", rely=3, relx=39, width=30, begin_entry_at=9)
            self.email   = form.add(npyscreen.TitleText, name="Email:", rely=5, relx=4, width=40, begin_entry_at=9)
            self.opt_out = form.add(npyscreen.TitleMultiSelect, relx=4, rely=6, value=[0,1], name="Email opt-out:", width=10, max_height=3,
                             values=[OPT_OUT_CON_INFO, OPT_OUT_SURVEY], scroll_exit=True)
            self.phone   = form.add(npyscreen.TitleText, name="Phone:", relx=4, rely=9, width=30, begin_entry_at=9)
            self.add1    = form.add(npyscreen.TitleText, name="Address 1:", relx=4, rely=10, width=60, begin_entry_at=13)
            self.add2    = form.add(npyscreen.TitleText, name="Address 2:", relx=4, rely=11, width=60, begin_entry_at=13)
            self.city    = form.add(npyscreen.TitleText, name="City:", rely=12, relx=4, width=30, begin_entry_at=8)
            self.state   = form.add(npyscreen.TitleText, name="State:", rely=12, relx=35, width=20, begin_entry_at=9)
            self.zip     = form.add(npyscreen.TitleText, name="ZIP:", rely=13, relx=4, width=20, begin_entry_at=8)
            self.country = form.add(npyscreen.TitleText, name="Country:", rely=14, relx=4, width=40, begin_entry_at=11)
            self.dob     = form.add(npyscreen.TitleDateCombo, name = "Date of Birth:", rely=15, relx=4, begin_entry_at=20)
            form.add(npyscreen.FixedText, value="(Press spacebar to set DOB)", rely=16, relx=4)
            self.agreement   = form.add(npyscreen.TitleSelectOne, relx=4, rely=18, value=[1,], name="I agree to abide by Furthemore's Code of Conduct", width=10, max_height=3,
                                               values = ["Yes", "No"], scroll_exit=True) #callbacks don't work, npyscreen is stupid

            form.edit()
        except Exception as e:
            print(str(e))

        while (self.agreement.values[self.agreement.value[0]].lower() != "yes"):
            npyscreen.notify_confirm("You need to agree to Furthemore's Code of Conduct to proceed.", title="Use tab and arrow keys")
            form.edit()

        if (VALIDATION_ENABLED):
            while (not self.isValid()):
                form.edit()

        try:
            additional_form    = RegistrationForm(name = "Use tab and arrow keys")
            self.badge              = additional_form.add(npyscreen.TitleText, name="Badge Name:", rely=2, relx=4, width=30, begin_entry_at=14)

            # format the registration levels for display, including option information
            level_values = []
            # we also need to create a mapping of displayed descs to IDs, because npyscreen is garbage.
            # It only returns the name as the selected object, not an ID...even though it accepts an ID in the value array at creation
            # (and value is subsequently inaccurate)
            level_id_desc_mapping = []
            for json_level in self.levels:
                level_desc = "{} (${})".format(json_level['name'], json_level['base_price'])
                if 'options' in json_level and len(json_level['options']) > 0 and json_level['options'][0]['active']:
                    for option in json_level['options']:
                        level_desc += " w/ {}".format(option['name'])

                level_values.append(level_desc)
                level_id_desc_mapping.append({ 'id' : json_level['id'], 'display_desc' : level_desc })

            self.level   = additional_form.add(npyscreen.TitleSelectOne, relx=4, value=[0,], name="Sponsorship Level:", width=10, values = level_values, scroll_exit=True)
            additional_form.edit()
        except Exception as e:
            print(str(e))

        if (VALIDATION_ENABLED):
            while (not self.isAdditionalValid()):
                additional_form.edit()

        data = {}
        attendee = {}
        price_level = {}
        attendee['firstName']       = self.first.value
        attendee['lastName']        = self.last.value
        attendee['address1']        = self.add1.value
        attendee['address2']        = self.add2.value
        attendee['city']            = self.city.value
        attendee['state']           = self.state.value
        attendee['country']         = self.country.value
        attendee['postal']          = self.zip.value
        attendee['phone']           = self.phone.value
        attendee['email']           = self.email.value
        attendee['onsite']          = 'true'
        attendee['birthdate']       = self.dob.value.isoformat()
        attendee['volDepts']        = ''
        attendee['asl']             = ''
        attendee['badgeName']       = self.badge.value
        selected_values = self.opt_out.get_selected_objects()
        if selected_values and OPT_OUT_CON_INFO in selected_values:
            attendee['emailsOk']    = 'true'
        else:
            attendee['emailsOk']    = 'false'
        if selected_values and OPT_OUT_SURVEY in selected_values:
            attendee['surveyOk']    = 'true'
        else:
            attendee['surveyOk']    = 'false'

        # once again, npyscreen is stupid
        selected_values = self.level.get_selected_objects()
        for available_level in level_id_desc_mapping:
            if available_level['display_desc'] == selected_values[0]:
                price_level['id'] = available_level['id']
                break

        price_level['options']      = []
        data['priceLevel']          = price_level
        data['attendee']            = attendee
        data['jersey']              = {}
        data['event']               = 'Furthemore 2018'

        cart_success = False
        try:
            cart_headers = REQ_HEADERS
            cart_headers['X-CSRFToken'] = self.session.cookies.get('csrftoken')
            r = self.session.post(CART_URL, headers=cart_headers, json=data)
            if r.json()['success'] is True:
                cart_success = True
        except Exception as e:
            npyscreen.notify_confirm("Sorry, but an exception occurred while trying to add the badge to your cart: " + str(e), title="Use tab and arrow keys")

        checkout_success = False
        if cart_success:
            try:
                checkout_headers = cart_headers
                checkout_payload = {'onsite':True,'billingData':{},'charityDonation':'','orgDonation':''}
                r = self.session.post(CHECKOUT_URL, headers=checkout_headers, json=checkout_payload)
                if r.json()['success'] is True:
                    checkout_success = True
            except Exception as e:
                npyscreen.notify_confirm("Sorry, but something went wrong while trying to checkout your cart: " + str(e), title="Use tab and arrow keys")

        if checkout_success:
            npyscreen.notify_confirm("Congratulations! You're registered for FurTheMore 2018! Please proceed to the cashier for payment.", title="Use tab and arrow keys")
        else:
            npyscreen.notify_confirm("Sorry, something went wrong during this process. Please try again or register without using this terminal.", title="Use tab and arrow keys")


if __name__ == "__main__":
    if PRINT_WELCOME_SCREEN:
        input("Press any key to start.")
        print(crow)
        print("Welcome to FurTheMore 2018!")
        somevar = input("Press any key to register.")
    App = TestApp()
    App.run()
